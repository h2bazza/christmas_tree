#include <WiFi.h>

const char* ssid = "digitalStudents";
const char* pass = "Study4Success!";

WiFiServer server(80);

String header;
int animationState = 0;

const int led1 = ?;
const int led2 = ?;
const int led3 = ?;
const int led4 = ?;
const int led5 = ?;
const int led6 = ?;
const int led7 = ?;
const int led8 = ?;
const int led9 = ?;
const int led10 = ?;
const int led11 = ?;
const int led12 = ?;
const int led13 = ?;
const int led14 = ?;
const int led15 = ?;
const int led16 = ?;
const int led17 = ?;
const int led18 = ?;
const int led19 = ?;
const int led20 = ?;
const int led21 = ?;
const int led22 = ?;
const int led23 = ?;
const int led24 = ?;
const int ledStar25 = ?;
const int ledStar26 = ?;
const int ledStar27 = ?;
const int ledStar28 = ?;
const int ledStar29 = ?;
const int ledStar30 = ?;


void setup() {
  Serial.begin(115200);

  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);
  pinMode(led6, OUTPUT);
  pinMode(led7, OUTPUT);
  pinMode(led8, OUTPUT);
  pinMode(led9, OUTPUT);
  pinMode(led10, OUTPUT);
  pinMode(led11, OUTPUT);
  pinMode(led12, OUTPUT);
  pinMode(led13, OUTPUT);
  pinMode(led14, OUTPUT);
  pinMode(led15, OUTPUT);
  pinMode(led16, OUTPUT);
  pinMode(led17, OUTPUT);
  pinMode(led18, OUTPUT);
  pinMode(led19, OUTPUT);
  pinMode(led20, OUTPUT);
  pinMode(led21, OUTPUT);
  pinMode(led22, OUTPUT);
  pinMode(led23, OUTPUT);
  pinMode(led24, OUTPUT);
  pinMode(ledStar25, OUTPUT);
  pinMode(ledStar26, OUTPUT);
  pinMode(ledStar27, OUTPUT);
  pinMode(ledStar28, OUTPUT);
  pinMode(ledStar29, OUTPUT);
  pinMode(ledStar30, OUTPUT);

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop() {

  WiFiClient client = server.available();

  if (client) {
    Serial.println("New Client.");
    String currentLine = "";

    while (client.connected()) {
      if (client.available()) {

        char c = client.read();
        Serial.write(c);
        header += c;

        if (c == '\n' ) {
          if (currentLine.length() == 0) {

            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            if (header.indexOf("GET /firstAnimation") >= 0) {
              Serial.println("Start first animation!");

              animationState = 1;
              stopAllAnimation();
              firstAnimation();

            } else if (header.indexOf("GET /secondAnimation") >= 0) {
              Serial.println("Start first animation!");

              animationState = 2;
              stopAllAnimation();
              secondAnimation();

            }// and others

            showHTMLPage();

            break;

          } else {
            currentLine = "";
          }

        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }

    header = "";

    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");

  }
}

void showHTMLPage(){
  //Implement page view
}

void stopAllAnimation(){
  //Spegnere tutti i LED e i buzzer

  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  digitalWrite(led5, LOW);
  digitalWrite(led6, LOW);
  digitalWrite(led7, LOW);
  digitalWrite(led8, LOW);
  digitalWrite(led9, LOW);
  digitalWrite(led10, LOW);
  digitalWrite(led11, LOW);
  digitalWrite(led12, LOW);
  digitalWrite(led13, LOW);
  digitalWrite(led14, LOW);
  digitalWrite(led15, LOW);
  digitalWrite(led16, LOW);
  digitalWrite(led17, LOW);
  digitalWrite(led18, LOW);
  digitalWrite(led19, LOW);
  digitalWrite(led20, LOW);
  digitalWrite(led21, LOW);
  digitalWrite(led22, LOW);
  digitalWrite(led23, LOW);
  digitalWrite(led24, LOW);
  digitalWrite(ledStar25, LOW);
  digitalWrite(ledStar26, LOW);
  digitalWrite(ledStar27, LOW);
  digitalWrite(ledStar28, LOW);
  digitalWrite(ledStar29, LOW);
  digitalWrite(ledStar30, LOW);
  
}

void firstAnimation(){
  //Implementare animazione

  int i;
  for(i = 0; i < 25; i++){
    digitalWrite(led+i, HIGH);
    delay(100);
  }

  for(i = 24; i >= 0; i--){
    digitalWrite(led+i, LOW);
    delay(100);
  }
  
  
}

void secondAnimation(){
  //Implementare animazione
}

